// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/presets/ERC1155PresetMinterPauser.sol";

/// @custom:security-contact security@schnizzl.biz
contract SchnizzlBizCadastre is ERC1155PresetMinterPauser {
    uint256 public constant PARCEL = 0;

    constructor()
        ERC1155PresetMinterPauser("https://schnizzl.biz/parcel/{id}.json")
    {
        _mint(msg.sender, PARCEL, 10**18, "");
    }
}
