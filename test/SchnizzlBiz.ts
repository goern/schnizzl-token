import { ethers, waffle } from "hardhat";
import chai from "chai";

import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

// eslint-disable-next-line node/no-missing-import
import { SchnizzlBiz } from "../typechain/SchnizzlBiz";
// eslint-disable-next-line camelcase
import { SchnizzlBiz__factory } from "../typechain/factories/SchnizzlBiz__factory";

const { deployContract } = waffle;
const provider = waffle.provider;

const { expect } = chai;

describe("SchnizzlBiz", function () {
    let szb: SchnizzlBiz;
    let owner: SignerWithAddress;

    beforeEach(async () => {
        [owner] = await ethers.getSigners();

        const SchnizzlBizFactory = (await ethers.getContractFactory(
            "SchnizzlBiz"
            // eslint-disable-next-line camelcase
        )) as SchnizzlBiz__factory;
        szb = await SchnizzlBizFactory.deploy();
        await szb.deployed();

        // eslint-disable-next-line no-unused-expressions
        expect(szb.address).to.properAddress;
    });

    it("should have an initial totalSupply of 37000000", async () => {
        const tS = await szb.totalSupply();

        expect(tS.toString(), "37000000");
    });
});
