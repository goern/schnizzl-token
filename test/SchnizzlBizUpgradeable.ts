import { ethers, waffle, upgrades } from "hardhat";
import chai from "chai";

import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

// eslint-disable-next-line node/no-missing-import
import { SchnizzlBiz } from "../typechain/SchnizzlBiz";
// eslint-disable-next-line camelcase
import { SchnizzlBiz__factory } from "../typechain/factories/SchnizzlBiz__factory";

const { deployContract } = waffle;
const provider = waffle.provider;

const { expect } = chai;

describe("SchnizzlBizUpgradeable", function () {
    let szb: SchnizzlBiz;
    let owner: SignerWithAddress;
    let addrs: SignerWithAddress[];

    beforeEach(async () => {
        [owner, ...addrs] = await ethers.getSigners();

        const SchnizzlBizFactory = (await ethers.getContractFactory(
            "SchnizzlBiz"
            // eslint-disable-next-line camelcase
        )) as SchnizzlBiz__factory;

        szb = (await upgrades.deployProxy(SchnizzlBizFactory, [], {
            initializer: "initialize",
        })) as SchnizzlBiz;

        await szb.deployed();
    });

    it("should have an initial totalSupply of 37000000", async () => {
        const tS = await szb.totalSupply();
        expect(tS.toString(), "37000000");
    });
});
