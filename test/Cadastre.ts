import { ethers, waffle } from "hardhat";
import chai from "chai";

import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

// eslint-disable-next-line node/no-missing-import
import { SchnizzlBizCadastre } from "../typechain/SchnizzlBizCadastre";
// eslint-disable-next-line camelcase
import { SchnizzlBizCadastre__factory } from "../typechain/factories/SchnizzlBizCadastre__factory";

const { deployContract } = waffle;
const provider = waffle.provider;

const { expect } = chai;

describe("SchnizzlBizCadastre", function () {
    let c: SchnizzlBizCadastre;
    let owner: SignerWithAddress;

    beforeEach(async () => {
        [owner] = await ethers.getSigners();

        const SchnizzlBizCadastreFactory = (await ethers.getContractFactory(
            "SchnizzlBizCadastre"
            // eslint-disable-next-line camelcase
        )) as SchnizzlBizCadastre__factory;
        c = await SchnizzlBizCadastreFactory.deploy();
        await c.deployed();

        // eslint-disable-next-line no-unused-expressions
        expect(c.address).to.properAddress;
    });

    it("should have 100000000000000 Parzels", async () => {
        const tS = await c.balanceOf(owner.address, 0);

        expect(tS._isBigNumber);
        expect(tS.eq(100000000000000));
        // FIXME have a real hard look if this really works
    });
});
